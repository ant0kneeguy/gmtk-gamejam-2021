from SuperLove.Globals import *

def testFunc(func):
	def wrapper(*args, **kwargs):
		print('\n' + '#' * 80)
		print(f"Testing [{func.__name__}]")
		print('#' * 80 + '\n')

		result = func(*args, **kwargs)

		print('\n' + '#' * 80)
		print(f"Test Complete [{func.__name__}]")
		print('#' * 80 + '\n\n')

		return result

	return wrapper

@testFunc
def testConverters():

	print("Test getTraitFromType()")
	print('-' * 80)
	for types in TRAIT_TYPES.values():
		for type_ in types:
			print(f"{type_} = {getTraitFromType(type_)}")

	print()


	print("Test getTypeFromTrait()")
	print('-' * 80)
	for trait in TRAIT_TYPES.keys():
		value = random.randrange(0,10) + 1
		print (f"{value} {trait} = {getTypeFromTrait(trait, value)}")

		value = random.randrange(10,20) + 1
		print (f"{value} {trait} = {getTypeFromTrait(trait, value)}")

	print()


	print("Test getTraitValue()")
	print('-' * 80)
	for types in TRAIT_TYPES.values():
		for type_ in types:
			value = random.randrange(0,10) + 1
			print(type_, value, end=' -> ')
			trait, absValue = getTraitValue(type_, value)
			print(trait, absValue)

	print()



	print("Test getTypeValue()")
	print('-' * 80)
	for trait in TRAIT_TYPES.keys():
		value = random.randrange(0,10) + 1
		print(trait, value, end=' -> ')
		type, absValue = getTypeValue(trait, value)
		print(type, absValue)

		value = random.randrange(10,20) + 1
		print(trait, value, end=' -> ')
		type, absValue = getTypeValue(trait, value)
		print(type, absValue)

	print()





@testFunc
def testUIElement():
	from SuperLove.UI import UIElements, Colour

	pygame.init()
	screen = pygame.display.set_mode((1600,1200))


	parent_element = UIElements.UIElement((100, 100), (200, 200))
	parent_element.setColour((0, 0, 0))
	def changeToRed(self):
		self.setColour((255, 0, 0))
	parent_element.setOnClickHandler(changeToRed)


	bottomRow = parent_element.addRow("top", 100)
	bottomRow.setColour(Colour.GREEN)

	leftColumn = parent_element.addColumn("left", 100)
	leftColumn.setColour(Colour.BLUE)


	clickable_rect = pygame.Rect(parent_element.pos, parent_element.size)

	while True:
		screen.fill((255, 255, 255))

		parent_element.draw(screen)
		pygame.display.update()

		if pygame.event.get(KEYUP):
			break

		if pygame.event.get(MOUSEBUTTONUP):
			if clickable_rect.collidepoint(pygame.mouse.get_pos()):
				parent_element.onClick()

	print("Tests succeeded :)")
#test harness





@testFunc
def testSuperLoveApp():
	from SuperLove.UI import UIElements
	from SuperLove.UI import SuperLoveApp

	pygame.init()
	screen = pygame.display.set_mode((1600,1200))


	super_love_app = SuperLoveApp.initSuperLoveApp(screen)


	while True:

		super_love_app.draw(screen)
		pygame.display.update()

		if pygame.event.get(KEYUP):
			break

		if pygame.event.get(MOUSEBUTTONUP):
			super_love_app.onClick()

	print("Tests succeeded :)")
#test harness





@testFunc
def testDate():
	from SuperLove.DateEngine import Date

	# Remember to load quirks before you load chars
	Date.Quirks.loadQuirksFromFile()
	Chars = Date.loadCharsFromFile()

	CharA, CharB = random.sample(list(Chars.values()), 2)

	D = Date.Date(CharA, CharB)

	D.runDate()

@testFunc
def testCharecter():
	from SuperLove.DateEngine import Character
	Character.Quirks.loadQuirksFromFile()
	chars = Character.loadCharsFromFile()

	print(f"All Characters from table: '{Character.CHAR_TABLE_FILEPATH}'")
	print('-' * 80)
	for char in chars.values():
		string = repr(char)
		if not char.avatar.is_file():
			string += f" Warning! Avatar missing ({char.avatar})"
		print(string)


@testFunc
def testPersonality():
	from SuperLove.DateEngine import Personality

	# Remember to load quirks before you can create personalities (with Quirks)
	Personality.Quirks.loadQuirksFromFile()

	# P1 = Personality.Personality(M=18, W=2, R=17, quirks="generate")
	# P2 = Personality.Personality(M=1, W=16, R=17, quirks="generate")

	print("This time our 2 randomly generated personalities are:")
	print('-' * 80)
	P1 = Personality.Personality(quirks="generate")
	P2 = Personality.Personality(quirks="generate")

	print(P1)
	print(P2)


@testFunc
def testQuirks():
	from SuperLove.DateEngine import Quirks

	Qs = Quirks.loadQuirksFromFile()

	print(f"All Quirks from table: '{Quirks.QUIRK_TABLE_FILEPATH}'")
	print('-' * 80)
	for q in Qs.values():
		print(repr(q))


if __name__ == "__main__":
	# Initialization
	pygame.init()

	testConverters()

	testQuirks()

	testPersonality()

	testCharecter()

	testDate()

	# Test UIElement()
	testUIElement()

	# Test SuperLoveApp
	#testSuperLoveApp()

	print("ALL SYSTEMS ARE GO!")
