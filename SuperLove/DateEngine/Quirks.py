from SuperLove.Globals import *

import csv

POOL = {} # Quirk Pool

QUIRK_TABLE_FILEPATH = Path("tables/Character Traits - Quirks.csv")

# Loads Quirks from file
def loadQuirksFromFile(filePath=QUIRK_TABLE_FILEPATH, update=True):
	quirks = {}
	with open(filePath) as file:
		reader = csv.reader(file)

		header = next(reader)
		if loadQuirksFromFile.DB: print(header)

		# Find which columns contain the data we are after
		iName 	= header.index("Quirk")
		iDevID 	= header.index("Quirk ID Code")
		iType 	= header.index("Trait Type") # Trait type of the quirk
		iQuirk 	= header.index("Type") # Quirk type i.e. Standard, Wildcard etc.

		iDesc 	= header.index("Date Description (for twitter page)")
		iEffect = header.index("Effect")
		iQn 	= header.index("Question")
		iAns 	= header.index("Answer")

		for row in reader:
			if loadQuirksFromFile.DB: print(row)
			
			# if row[iName] == 
			name 	= row[iName]
			devID 	= row[iDevID]
			tType 	= row[iType]
			qType 	= row[iQuirk]

			if qType == Q_STANDARD:
				quirk = Standard(name, tType)
			
			elif qType == Q_SPECIAL:
				quirk = Special(name, tType)

			elif qType == Q_WILDCARD:
				quirk = WildCard(name, tType)

			else:
				raise Exception("Unrecognised quirk type")

			if not isinstance(quirk, Quirk): # Skip if it's an unimplemented quirk
				quirk.desc 		= row[iDesc]
				quirk.effect 	= row[iEffect]
				quirk.Qn 		= row[iQn]
				quirk.ans 		= row[iAns]

			quirks[name] = quirk

			

	if update: POOL.update(quirks)
	return quirks

loadQuirksFromFile.DB = False # Toggle debugging for the function above



# Abstract Base class (only for sub-typing)
class Quirk:
	DB = True # Turn Debugging on/off for this class
	qType = Q_STANDARD

	def __init__(self, name=None, tType=ERR):
		self.name = name
		self.type = tType
		self.trait = getTraitFromType(tType)

	def __repr__(self):
		return f"{self.name} ({self.type} {self.qType})"


	def __str__(self):
		return "Quirk: " + repr(self) 



# Quirk Roll
class Standard(Quirk):
	qType = Q_STANDARD
	def execute(self, agent, target):
		difficulty = abs(agent.traits[self.trait] - target.traits[self.trait])

		roll = rollDice()
		success = difficulty <= roll

		diff = abs(difficulty - roll) 

		if   diff <= 5:		score = 5
		elif diff <= 10:	score = 15
		else: 				score = 20

		score = score if success else -score

		if self.DB:
			print(f"Playing {self.type} Quirk")
			print(f"|{difficulty} - {roll}| = {diff}")
			print("SUCCESS" if success else "FAILURE")

		return score



class Special(Quirk):
	qType = Q_SPECIAL
	def execute(self, agent, target):
		score = target.traits[self.trait] 

		if self.trait != target.types[self.trait]: score = -score

		return score


class WildCard(Quirk):
	qType = Q_WILDCARD
	def execute(self, agent, target):
		score = 0 

		print("That was WILD!")

		return score