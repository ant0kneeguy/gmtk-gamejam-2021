from SuperLove.Globals import *
from .Character import *


class Date:
	DB = True # Turn Debugging on/off for this class
	def __init__(self, character0, character1):
		if self.DB: 
			print(f"Creating a date for {character0.name} & {character1.name}")


		# Overall date score
		self.score = 0

		# Tuple of the Characters on the date
		self.chars = (character0, character1)

		# List of quirks which each Character can use during the date 
		self.moves = [character0.personality.quirks.copy(), character1.personality.quirks.copy()]
		random.shuffle(self.moves[0])
		random.shuffle(self.moves[1])

		# List for keeping track of the quirks that have been played
		self.plays = [[], []] 

		# Current player (0 or 1)
		self.current = 0
		self.other = self.current ^ 1  # XOR with '1' to invert it 


	def runDate(self, truns=4):
		# Date Loop 
		for turn in range(truns):
			self.makeYourPlay()


	def makeYourPlay(self):		
		# Execute one turn i.e. play one Quirk
		print(f"{self.chars[self.current].name}'s turn")
		hero = self.chars[self.current]
		victim = self.chars[self.other]

		quirk = self.moves[self.current].pop()
		self.plays.append(quirk)

		playScore = quirk.execute(hero.personality, victim.personality)
		self.score += playScore
		if self.DB: print(f"SCORE: {playScore} (Total {self.score})\n")

		self.swapCurrent()


	def swapCurrent(self):
		self.other = self.current
		self.current ^= 1 # XOR with '1' to invert it 






