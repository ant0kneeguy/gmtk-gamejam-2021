from SuperLove.Globals import *
from .Personality import *

import csv, re
from copy import copy, deepcopy

CHAR_TABLE_FILEPATH = Path("tables/Character Traits - Characters.csv")
AVATARS_FOLDER = Path("assets/avatars")
AVATAR_PLACEHOLDER = "impossibledave.png"

# Loads Characters from file
def loadCharsFromFile(filePath=CHAR_TABLE_FILEPATH):
	characters = {}
	with open(filePath) as file:
		reader = csv.reader(file)

		header = next(reader)
		if loadCharsFromFile.DB: print(header)

		# Find which columns contain the data we are after
		iReady 	= header.index("Asset Complete")
		iDevID 	= header.index("Character Dev IDs")
		iName 	= header.index("Name")
		iBio 	= header.index("Bio")
		iGender = header.index("Gender")
		iAvatar = header.index("Avatar")


		iTraits = {}
		for trait in TRAIT_TYPES.keys():
			iTraits[trait] = header.index(trait)

		iQuirks = {}
		for i in range(1, Personality.quirkNum+1):
			iQuirks[i] = header.index(f"Quirk {i}")


		for row in reader:
			if loadCharsFromFile.DB: print(row)

			if row[iReady] == "TRUE":

				name 	= row[iName]
				bio 	= row[iBio]
				gender 	= row[iGender]
				avatar  = row[iAvatar]

				# Read Traits
				traits = {}
				for trait, i in iTraits.items():
					if row[i] == 'x': continue # Skip if Quirk is missing

					match = re.match(r"\s*(\d+)\s+(\w+)\s*", row[i])
					if not match: raise Exception(f"Invalid trait format: {row[i]}")

					type, typeVal = match[2], int(match[1])
					traitCheck, traitVal = getTraitValue(type, typeVal)

					if trait == traitCheck:
						traits[trait] = traitVal
					else:
						raise Exception(f"Invalid trait type: {row[i]}")

				global M, W, R
				personality = Personality(traits[M], traits[W], traits[R])

				# Read Quirks
				for i in iQuirks.values():
					quirkName = row[i].strip() if row[i] != 'x' else None

					personality.addQuirk(Quirks.POOL[quirkName]) 


				characters[name] = Character(name, bio, gender, personality, avatar)

		return characters

loadCharsFromFile.DB = False # Toggle debugging for the function above




class Character:
	def __init__(self, name, bio=None, gender=None, personality=None, avatar=None):
		self.name = name
		self.bio = bio
		self.gender = gender

		avatar = avatar if avatar else AVATAR_PLACEHOLDER
		self.avatar = AVATARS_FOLDER / avatar

		self.personality = personality if personality else Personality()


	def __len__(self):
		return len(self.personality)


	def __repr__(self):
		string = f"Character: {self.name}"

		# Print flags to represent characters attributes
		attrs = ''
		if self.bio: 	attrs += 'B' 
		if self.gender: attrs += 'G'
		if attrs: string += f" ({attrs})"

		return string

	def __str__(self):
		nameStr = string = ""

		for name, var in vars(self).items(): # Gets all fields of this instance
			if name == "name":
				nameStr = f"{repr(self)}\n"

			elif name == "personality":
				personality = str(var)
				personality.replace('\n', '\n\t')
				string += personality

			else:
				string += f"{name}: {var}\n"

		return nameStr + string
