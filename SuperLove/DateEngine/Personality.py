from SuperLove.Globals import *
from . import Quirks

import random

# Class which stores Traits and Quirks
class Personality:
	traitNum = 3 # Number of traits per character
	minT = 0
	maxT = 20

	quirkNum = 6 # Number of quirks per character

	def __init__(self, M=None, W=None, R=None, quirks=None):
		# Generate traits if none are provided
		if M is None: M = rollDice()
		if W is None: W = rollDice()
		if R is None: R = rollDice()

		self.traits  = {	
						MORALITY: 	M,
						WEIRDNESS: 	W,
						ROMANCE: 	R,
					  }

		self.types  = {	
						MORALITY: 	getTypeValue(MORALITY, M),
						WEIRDNESS: 	getTypeValue(WEIRDNESS, W),
						ROMANCE: 	getTypeValue(ROMANCE, R),
					  }

		if quirks == "generate":
			quirks = random.sample(list(Quirks.POOL.values()), self.quirkNum)

		elif quirks == None:
			quirks = []

		if not isinstance(quirks, list):
			raise AssertionError()

		self.quirks = quirks


	# Properties (Just for convenience)
	# (might've went a little over board with these but whatever ¯\_(ツ)_/¯)
	@property
	def Morality(self):  return self.traits[MORALITY]

	@property
	def Weirdness(self): return self.traits[WEIRDNESS]

	@property
	def Romance(self):   return self.traits[ROMANCE]


	@property
	def M(self):  return self.traits[MORALITY]

	@property
	def W(self): return self.traits[WEIRDNESS]

	@property
	def R(self):   return self.traits[ROMANCE]



	@property
	def Mtype(self): return self.types[MORALITY]

	@property
	def Wtype(self): return self.types[WEIRDNESS]

	@property
	def Rtype(self): return self.types[ROMANCE]



	def __getitem__(self, key):
		return self.quirks[key]
  
	def __setitem__(self, key, newvalue):
		self.quirks[key] = newvalue


	def __len__(self):
		return len(self.quirks)


	def __repr__(self):
		return f"Personality [M:{self.M} W:{self.W} R:{self.R}]({len(self)})"

	def __str__(self):
		string = "Personality:\n"
		string += f"{self.M}:\t{self.Mtype[0]}\n"
		string += f"{self.W}:\t{self.Wtype[0]}\n"
		string += f"{self.R}:\t{self.Rtype[0]}\n"
		string += f"Quirks ({len(self.quirks)}):\n"
		for i, quirk in enumerate(self.quirks):
			string += f"\t{repr(quirk)}\n"
		return string


	def addQuirk(self, quirk):
		if not issubclass(type(quirk), Quirks.Quirk):
			raise Exception("Must be a Quirk or a list of Quirks")

		self.quirks.append(quirk)



