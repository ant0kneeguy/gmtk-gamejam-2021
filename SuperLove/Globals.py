print("Importing Globals")
import pygame, random
from pygame.locals import *

from pathlib import Path

#				R	G	B
WHITE		= (255,255,255)
BLACK		= (  0,  0,  0)
BLUE		= (  0,  0,255)
RED			= (255,  0,  0)
GREEN		= (  0,255,  0)
DARK_GREEN	= (  0,155,  0)
DARK_GRAY	= ( 40, 40, 40)
LIGHT_GRAY	= (211,211,211)
PINK		= (255,105,180)
PURPLE		= (128,  0,128)
INDIGO		= ( 75,  0,130)



# Traits
M = MORALITY 	= "Morality"
W = WEIRDNESS 	= "Weirdness"
R = ROMANCE 	= "Romance"
ERR = "UNDEFINED"

TRAIT_TYPES = {
	M: ["Good", "Villain"],
	W: ["Weird", "Normal"],
	R: ["Romantic", "Hedonist"]
}

THRESHOLD = 10 # Threshold for deciding trait type

# Quirks
Q_STANDARD = "Standard"
Q_SPECIAL  = "Special"
Q_WILDCARD = "Wildcard"
QUIRK_TYPES = (Q_STANDARD, Q_SPECIAL, Q_WILDCARD)

############################## Utility Functions ###############################

# Find Trait given Type i.e. Evil -> Morality
def getTraitFromType(type):
	for trait, types in TRAIT_TYPES.items():
		if type in types: return trait
	return ERR

# Find Type given Trait value i.e. Morality 1 -> Evil 
def getTypeFromTrait(trait, value):
	return TRAIT_TYPES[trait][0] if value > THRESHOLD else TRAIT_TYPES[trait][1]




# Convert from type value to trait value i.e. Evil 10 -> Morality 1
def getTraitValue(type, value):
	trait = getTraitFromType(type)
	idx = TRAIT_TYPES[trait].index(type)

	if idx == 0:return (trait, THRESHOLD + value) 
	else:		return (trait, THRESHOLD - (value - 1))

# Convert from trait value to type value i.e. Morality 1 -> Evil 10
def getTypeValue(trait, value):
	type = getTypeFromTrait(trait, value)
	idx = TRAIT_TYPES[trait].index(type)

	if idx == 0:return (type, value - THRESHOLD)
	else:		return (type, THRESHOLD - (value - 1))



def rollDice(D=20):
	return random.randrange(0,20) + 1

def percentage(num, max):
	return (num / max) * 100 if (max != 0) else 100
