from SuperLove.Globals import *
from pygame.locals import *

from .UI import ui, UIElements

import sys


def checkForMousePress():
	mouseUpEvents = pygame.event.get(MOUSEBUTTONUP)

	if len(mouseUpEvents) == 0:
		return None
	elif len(mouseUpEvents) > 0:
		return mouseUpEvents[0]
#checkForMousePress()

def checkForKeyPress():
	if len(pygame.event.get(QUIT)) > 0:
		terminate()

	keyUpEvents = pygame.event.get(KEYUP)

	if len(keyUpEvents) == 0:
		return None
	else:
		if keyUpEvents[0].key == K_ESCAPE:
			terminate()
		else:
			return keyUpEvents[0].key
#checkForKeyPress()


def handleMouseEvent(event):
	for element in UIElements.UI_ELEMENTS:
		# print(type(element.collisionBox))
		if element.collisionBox.collidepoint(pygame.mouse.get_pos()):
			element.onClick()
#handleMouseEvent()


def handleKeyEvent(event):
	pass

#handleKeyEvent

	


def terminate():
	pygame.quit()
	sys.exit()
#terminate()