# Copyright 2012 by Al Sweigart al@inventwithpython.com
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from SuperLove.Globals import *

print("Initialising pygame")
# Initialization
pygame.init()

from SuperLove import EventHandlers
from SuperLove.UI import ui

import sys


# Initialise the game and run the game loop
def run():
	ui.initialise()

	ui.CURRENT_SCREEN = 'Title Screen'

	print("Starting gameLoop")
	while True:
		gameLoop()
#run()


# main game loop
def gameLoop():
	# Get input
	key_pressed = EventHandlers.checkForKeyPress()
	mouse_pressed = EventHandlers.checkForMousePress()


	# Handle input events
	if ui.CURRENT_SCREEN == 'Title Screen':
		if key_pressed or mouse_pressed:
			print("Starting Desktop...")
			ui.CURRENT_SCREEN = 'Desktop'
			
	elif ui.CURRENT_SCREEN == 'Desktop':
		if key_pressed or mouse_pressed:
			EventHandlers.handleMouseEvent(mouse_pressed)
			EventHandlers.handleKeyEvent(key_pressed)


	# Render to screen
	ui.displayCurrentScreen()
	pygame.display.update()
#gameLoop()




if __name__ == '__main__':
	# run, Forrest ...
	run()
