from SuperLove.Globals import *

from .UIElements import BASIC_FONT, UIElement, App
from SuperLove.DateEngine import Date


class ContactList(UIElement):
	DB = False

	bgColour = (245,245,220)

	pos = (0, 0) # Relative to its App Window
	width = 350

	def __init__(self, appSurf, pos=None, size=None):
		self.appSurf = appSurf

		if pos: self.pos = pos

		if size is None:
			self.height = self.appSurf.get_height()
		else:
			self.width, self.height = size

		self.size = (self.width, self.height)
		self.surf = pygame.Surface(self.size)

		# Read Character & Quirk tables 
		Date.Quirks.loadQuirksFromFile()
		self.qPool = Date.Quirks.POOL
		self.contacts = Date.loadCharsFromFile()

		self.initListItems()


	def __len__ (self):
		return len(self.contacts)


	def __getitem__(self, key):
		return self.contacts[key]

	def __setitem__(self, key, value):
		self.contacts[key] = value


	def __repr__(self):
		return f"ContactList [{len(self)}]"

	def __str__(self):
		string = f"{repr(self)}:\n"
		for char in self.contacts.values():
			string += f"{repr(char)}\n"
		return string


	def initListItems(self):
		self.listItems = {}
		pos = [0,0]
		for name, contact in self.contacts.items():
			if self.DB: print(f"Adding item for {contact.name} at {pos}")
			self.listItems[name] = ListItem(self.surf, contact, pos.copy())
			pos[1] += ListItem.height
			if pos[1] >= self.height:
				raise Exception


	def display(self):
		self.drawBg()

		self.drawItems()

		self.appSurf.blit(self.surf, self.pos)


	def drawBg(self):
		# list background
		self.surf.fill(self.bgColour)


	def drawItems(self):
		for item in self.listItems.values():
			item.display()


class ListItem(UIElement):
	height = 32 # This should be the heigh of the avatar (I think)
	width = ContactList.width
	def __init__(self, listSurf, char, pos):
		self.listSurf = listSurf

		self.pos = pos

		self.width = listSurf.get_width()

		self.size = (self.width, self.height)
		self.surf = pygame.Surface(self.size)

		self.char = char

		# Initialise contact avatar
		self.img = pygame.image.load(self.char.avatar)
		self.imgPos = (0, ((self.height - self.img.get_height()) // 2))
		
		# Initialise item name text
		self.nameSurf = BASIC_FONT.render(self.char.name, 1, BLACK)


	def display(self):
		self.drawBg()
		self.drawContents()

		# print(f"Displaying ContactList item for {self.char.name} at {self.pos}")
		self.listSurf.blit(self.surf, self.pos)


	def drawBg(self):
		# list background
		self.surf.fill(ContactList.bgColour)

	def drawContents(self):
		# Display contact avatar
		self.surf.blit(self.img, self.imgPos)

		# Display contact name to the right of image
		nameWidth = self.img.get_width() + 10
		nameHeight = (self.height - self.nameSurf.get_height()) // 2
		self.surf.blit(self.nameSurf, (nameWidth, nameHeight))
		





class ContactsApp(App):
	bgColour = RED
	contactsPos = (0, 0)

	def __init__(self, surf):
		self.cList = ContactList(surf)
		super().__init__(surf)




	def display(self):
		# print("Displaying ContactsApp")
		self.drawBg()
		self.drawAppContents()


	def drawAppContents(self):
		self.surf.fill(self.bgColour)
		
		self.cList.display()

if __name__ == "__main__":
	pygame.init()
	CL = ContactList(pygame.Surface((0,0)))
	print(CL)
