import pygame, UIElement, Colour, sys


# holds all icons on the desktop
desktop_icons = []


def createDesktop(screen):

	# wallpaper
	wallpaper = UIElement.UIElement((0, 0), screen.get_size())
	wallpaper.setColour(Colour.PURPLE)

	# taskbar
	taskbar_height = 50
	taskbar = UIElement.UIElement((0, screen_height - taskbar_height), (screen_width, taskbar_height), wallpaper)
	taskbar.setColour(Colour.DARK_GRAY)

	# icons
	heightBetweenIcons = 30
	widthBetweenIcons = 50

	contacts_pos = (widthBetweenIcons, heightBetweenIcons)
	contacts_thumbnail = pygame.image.load("../assets/contacts.png")
	contacts_icon = Icon("Contacts", contacts_thumbnail, contacts_pos, wallpaper)

	superlove_pos = (widthBetweenIcons, contacts_pos[1] + contacts_icon.getHeight() + heightBetweenIcons)
	superlove_thumbnail = pygame.image.load("../assets/superlove.png")
	superlove_icon = Icon("Super Love", superlove_thumbnail, superlove_pos, wallpaper)

	chatter_pos = (widthBetweenIcons, superlove_pos[1] + superlove_icon.getHeight() + heightBetweenIcons)
	chatter_thumbnail = pygame.image.load("../assets/chatter.png")
	chatter_icon = Icon("Chatter", chatter_thumbnail, chatter_pos, wallpaper)

	# return the parent element
	return wallpaper
#createDesktop()



class Icon(UIElement.UIElement):
	# string representing icon name
	name_string = None

	currently_selected = False

	def __init__(self, name_string, thumbnail, pos, parent):
		# '(0,0)' is because size will be set after name has been rendered
		super().__init__(pos, (0, 0), parent)

		self.name_string = name_string

		# create a thumbnail UI element using the provided image
		thumbnail_element = UIElement.UIElement(pos, (64, 64), self)
		thumbnail_element_width = thumbnail_element.getWidth()
		thumbnail_element.setImage(thumbnail)

		# create a name element using the provided name_string, to be displayed below the thumbnail
		global BASIC_FONT
		name_image = BASIC_FONT.render(name_string+".exe", 1, Colour.WHITE)
		name_image_width = name_image.get_width()
		name_element = UIElement.UIElement((pos[0] + thumbnail_element_width/2 - name_image_width/2, pos[1] + 64 + 10), name_image.get_size(), self)
		name_element.setImage(name_image)

		# add these elements as children
		self.children = [thumbnail_element, name_element]

		self.setWidth(name_image.get_width())
		self.setHeight(64 + name_image.get_height())
	#__init__()
 
	def __str__(self):
		string = "Icon:\n"
		string += f"Name: {self.name_string}\n"
		return string
	#__str__()
#class Icon





# Test harness
if __name__ == "__main__":
	import pygame, sys, UIElement, Colour
	from pygame.locals import *

	screen_width  = 1600
	screen_height = 1200
	screen = pygame.display.set_mode((screen_width, screen_height))
	pygame.init()
	BASIC_FONT = pygame.font.SysFont('Console', 16, True) # True makes it bold


	desktop = createDesktop(screen)


	while True:
		desktop.draw(screen)
		pygame.display.update()

		if pygame.event.get(KEYUP):
			break

		if pygame.event.get(MOUSEBUTTONUP):
			desktop.onClick()

	print("Tests succeeded :)")
#Test harness