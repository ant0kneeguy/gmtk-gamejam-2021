from SuperLove.Globals import *

print("Initialising Font")
BASIC_FONT = pygame.font.SysFont('Console', 16, True) # True makes it bold

UI_ELEMENTS = []
DESKTOP_ICONS = []

PLACEHOLDER_THUMBNAIL = Path('assets/cross.png')


class UIElement:
	parent         = None
	children       = None
	pos            = ('x', 'y') # relative to parent
	size           = ('w', 'h')
	surface        = None
	onClickHandler = None

	# Used by addRow() and addColumn() to know where there is free space to add
	# a new row/column without overlapping other children
	top    = None
	bottom = None
	left   = None
	right  = None

	def __init__(self, pos, size, parent=None):
		self.parent = parent
		if self.parent:
			self.parent.addChild(self)

		self.children = []
		
		self.size = size
		self.pos = pos

		self.top  = 0
		self.left = 0
		self.right  = self.size[0]
		self.bottom = self.size[1]
	#__init__

	def setWidth(self, w):
		self.size = (w, self.getHeight())
	#setWidth()

	def getWidth(self):
		return self.size[0]
	#getWidth()

	def setHeight(self, h):
		self.size = (self.getWidth(), h)
	#setHeight()

	def getHeight(self):
		return self.size[1]
	#getHeight()

	def setX(self, x):
		self.pos = (x, self.getY())
	#setX()

	def getX(self):
		return self.pos[0]
	#getX()

	def setY(self, y):
		self.pos = (self.getX(), y)
	#setY()

	def getY(self):
		return self.pos[1]
	#getY()

	def addChild(self, element):
		self.children.append(element)
	#addChild()

	# This 'column' is just another UI element, not its own class
	def addColumn(self, sideToAttachTo, thickness):
		x = self.left
		y = self.top
		w = self.right  - self.left
		h = self.bottom - self.top

		if sideToAttachTo == "left":
			w = thickness

			self.left += thickness

		elif sideToAttachTo == "right":
			x = self.right - thickness
			w = thickness

			self.right -= thickness

		else:
			return None

		return UIElement((x, y), (w, h), self)
	#addColumn


	# This 'row' is just another UI element, not its own class
	def addRow(self, sideToAttachTo, thickness):
		x = self.left
		y = self.top
		w = self.right  - self.left
		h = self.bottom - self.top


		if sideToAttachTo == "top":
			h = thickness

			self.top += thickness

		elif sideToAttachTo == "bottom":
			y = self.bottom - thickness
			h = thickness

			self.bottom -= thickness

		else:
			return None

		return UIElement((x, y), (w, h), self)
	#addColumn



	def removeChild(self, element):
		if element in self.children:
			self.children.remove(element)
	#removeChild

	def setImage(self, image):
		self.surface = image
		if self.size:
			self.surface = pygame.transform.scale(image, self.size)
	#setImage()

	def setColour(self, colour):
		self.surface = pygame.Surface(self.size)
		self.surface.fill(colour)
	#setColour()

	def setTransparency(self, number):
		self.surface.set_alpha(number)
	#setTransparency

	def draw(self, display_surface):
		# draw self first
		if self.surface:
			display_surface.blit(self.surface, self.pos)

		# then draw children on top
		if len(self.children) > 0:
			for child in self.children:
				child.draw(display_surface)
	#draw()

	# Remember that the function you define as a handler must have self
	# as a first argument!
	def setOnClickHandler(self, function):
		self.onClickHandler = function
	#setOnClickHandler()

	def onClick(self):
		for child in self.children:
			child_rect = pygame.Rect(child.pos, child.size)
			if child_rect.collidepoint(pygame.mouse.get_pos()):
				return child.onClick()
		if self.onClickHandler:
			return self.onClickHandler(self)
	#onClick()

#UIElement



class Icon:
	DB = True # Turn Debugging on/off for this class

	# string representing icon name
	name_string = None

	# object representing name below thumbnail
	name = None
	thumbnail = None

	# part that can be clicked on
	collisionBox = None

	#x-y coordinates of those elements
	namePos = (0, 0)
	thumbnailPos = (0, 0)
	collisionBoxPos = (0, 0)

	currentlySelected = False

	def __init__(self, name_string, link=None, thumbnail=PLACEHOLDER_THUMBNAIL):
		self.name_string = name_string

		self.link = link # Link to the corresponding app

		# Load in thumbnail image
		self.thumbnail = pygame.image.load(thumbnail)

		# global
		UI_ELEMENTS.append(self)
		DESKTOP_ICONS.append(self)
	#__init__()

	def __str__(self):
		string = "Icon:\n"
		string += f"Name: {self.name_string}\n"
		return string
	#__str__()


	def drawSelectionBox(self):
		for icon in DESKTOP_ICONS:
			if icon.currentlySelected:
				width = icon.collisionBox.w
				height = icon.collisionBox.h
				selectionBox = pygame.Surface((width, height))
				selectionBox.set_alpha(65)
				selectionBox.fill(BLUE)
				self.surf.blit(selectionBox, icon.collisionBoxPos)
	#drawSelectionBox()

	def onClick(self):
		if self.DB: print("Clicked on {} (Selected:{})".format(
						self.name_string, self.currentlySelected))

		if self.currentlySelected == False:
			# deselect all other icons
			for icon in DESKTOP_ICONS:
				icon.currentlySelected = False
			# select this icon
			self.currentlySelected = True
		else:
			if self.link: self.link()

	#onClick()

#class Icon





class Window:
	DB = True

	titleBarHeight = 50

	# Default values
	contentsPos = (0, titleBarHeight)
	contentsWidth = 0
	contentsHeight = 0

	def __init__(self, surf, pos, app):

		self.pos = pos

		self.appClass = app

		self.surf = surf
		self.width, self.height = surf.get_size()

		if self.DB: print("Opening Window", self.size)

		self.contents = pygame.Surface((self.width, self.height - self.titleBarHeight))

		# Create App instance for this window
		self.app = app(self.contents)

	#__init__()

	@property
	def size(self):
		return (self.width, self.height)


	def display(self):
		self.drawAppContents()

		# title bar (window name, close button, etc.)
		self.drawTttleBar()

	def drawAppContents(self):
		self.app.display()
		self.surf.blit(self.contents, self.contentsPos)

	def drawTttleBar(self):
		titleBar = pygame.Surface((self.width, self.titleBarHeight))
		titleBar.fill(BLACK)
		self.surf.blit(titleBar, (0,0))

#class Window


class WindowManager:
	spaceOnLeft = 175

	winPos = (spaceOnLeft, 0)

	maxWindows = 1


	def __init__(self, displaySurf, windowArea):
		self.displaySurf = displaySurf
		self.winSize = (windowArea.width - self.spaceOnLeft, windowArea.height)
		self.windowArea = windowArea

		self.openWindows = []


	def displayWindows(self):
		for window in self.openWindows:
			if isinstance(window, Window):
				window.display()
				self.displaySurf.blit(window.surf, self.winPos)
			else:
				window.displayWindow(self.winSize, self.winPos)


	def getAppLink(self, app):
		# Creates a way to open an app window without needing reference to this
		# instance of WindowManager (used for icons)
		def link():
			self.openWindows = [] # TEMP
			return self.getAppWindow(app)

		return link


	def getAppWindow(self, app):
		# Keep creating new windows until we reach our window cap
		if len(self.openWindows) < self.maxWindows:
			windowSurf = pygame.Surface(self.winSize)

			# Create a Window instance for the app
			new = Window(windowSurf, self.winPos, app)

			self.openWindows.append(new)
			return new

		else:
			# Start re-using windows if we run out
			return self.openWindows[-1]

#class WindowManager




class App(UIElement):
	bgColour = INDIGO

	def __init__(self, surf):
		self.surf = surf
		self.width, self.height = surf.get_size()

		self.drawAppWindow()

	def drawBg(self):
		# window background
		self.surf.fill(self.bgColour)


	def drawAppWindow(self):
		# window background (Solid colour)
		self.drawBg()

		# draw window contents
		self.display()

#class App
