from SuperLove.UI import UIElements, Colour




#								VARIABLES

space_on_left = 175   # based on the variable in UIElements:WindowManager class
title_bar_height = 50 # based on the variable in UIElements:Window class





#								FUNCTIONS

def initSuperLoveApp(display_surface):
	background_pos    = (space_on_left, title_bar_height)
	background_width  = display_surface.get_width()  - space_on_left
	background_height = display_surface.get_height() - title_bar_height
	background_size   = (background_width, background_height)
	background = UIElements.UIElement(background_pos, background_size, None)
	background.setColour(Colour.PINK)

	contacts_column_size = (background_width/4, background_height)
	contacts_column = UIElements.UIElement((0,0), contacts_column_size, background)
	contacts_column.setColour(Colour.WHITE)

	chat_row_pos_x  = contacts_column.getWidth()
	chat_row_pos_y  = background_height - 2*background_height/3
	chat_row_pos    = (chat_row_pos_x, chat_row_pos_y)
	chat_row_size_x = background.getWidth()  - contacts_column.getWidth()
	chat_row_size_y = 2*background.getHeight()/3
	chat_row_size   = (chat_row_size_x, chat_row_size_y)
	chat_row = UIElements.UIElement(chat_row_pos, chat_row_size, background)
	chat_row.setColour(Colour.LIGHT_GRAY)

	selected_contact_box_pos    = (contacts_column.getWidth(), 0)
	selected_contact_box_size_x = (background.getWidth() - contacts_column.getWidth())/3
	selected_contact_box_size_y = background.getHeight()/3
	selected_contact_box_size   = (selected_contact_box_size_x, selected_contact_box_size_y)
	selected_contact_box = UIElements.UIElement(selected_contact_box_pos, selected_contact_box_size, background)
	selected_contact_box.setColour(Colour.GREEN)

	stat_box_pos_x = selected_contact_box_pos[0] + selected_contact_box.getWidth()
	stat_box_pos   = (stat_box_pos_x, selected_contact_box_pos[1])
	stat_box_size  = selected_contact_box_size
	stat_box = UIElements.UIElement(stat_box_pos, stat_box_size, background)
	stat_box.setColour(Colour.BLUE)

	match_box_pos_x = stat_box_pos_x + stat_box.getWidth()
	match_box_pos   = (match_box_pos_x, stat_box_pos[1])
	match_box_size  = stat_box_size
	match_box = UIElements.UIElement(match_box_pos, match_box_size, background)
	match_box.setColour(Colour.RED)

	return background
#initElements()
