from SuperLove.Globals import *
from . import UIElements, ContactsApp

# We don't need to declare them as globals here (it's not like extern in C++)
# I've just left it here for clarity, as they are defined inside functions here
global DISPLAY_SURF, DISPLAY_HEIGHT, DISPLAY_WIDTH
global CURRENT_SCREEN, SCREENS

BG_COLOR = PURPLE

THUMBNAIL_HEIGHT = 64

Contacts 	= "Contacts"
SuperLove 	= "Super Love"
Chatter 	= "Chatter"
APP_NAMES = (Contacts, SuperLove, Chatter)




class LoginScreen:
	name = 'Title Screen'
	def __init__(self, surf):
		self.surf = surf
		self.width, self.height = surf.get_size()
	#__init__

	def display(self):
		gameLogo = pygame.image.load('assets/super_love_placeholder_logo.png')

		self.surf.fill(BG_COLOR)
		self.surf.blit(gameLogo, ((self.width - gameLogo.get_width())/2, 
								(self.height - gameLogo.get_height())/2))
	#display



class Desktop:
	name = 'Desktop'
	taskbarHeight = 50
	def __init__(self, surf):
		self.surf = surf
		self.width, self.height = surf.get_size()

		windowArea = pygame.Rect((0, 0), 
			(self.width, self.height - self.taskbarHeight))

		self.windowManager = UIElements.WindowManager(self.surf, windowArea)

		initIcons(self.windowManager)


	def display(self):
		self.drawWallpaper()
		self.drawSelectionBox()
		self.drawIcons()

		self.drawWindows()

		self.drawTaskbar()



	def drawWindows(self):
		self.windowManager.displayWindows()


	def drawWallpaper(self):
		self.surf.fill(BG_COLOR)
	#drawWallpaper()


	def drawTaskbar(self):
		global DARK_GRAY
		pygame.draw.rect(self.surf, DARK_GRAY, (0, self.height - self.taskbarHeight, 
												self.width, self.taskbarHeight))
	#drawTaskbar()



	# position and draw the icons
	def drawIcons(self):
		global DESKTOP_ICONS
		for icon in DESKTOP_ICONS:
			# draw an thumbnail
			self.surf.blit(icon.thumbnail, (icon.thumbnailPos[0], icon.thumbnailPos[1]))
			# draw icon name below
			self.surf.blit(icon.name, (icon.namePos[0], icon.namePos[1]))
	#drawIcons



	def drawSelectionBox(self):
		global DESKTOP_ICONS
		for icon in DESKTOP_ICONS:
			if icon.currentlySelected:
				width = icon.collisionBox.w
				height = icon.collisionBox.h
				selectionBox = pygame.Surface((width, height))
				selectionBox.set_alpha(65)
				selectionBox.fill(BLUE)
				self.surf.blit(selectionBox, icon.collisionBoxPos)
	#drawSelectionBox()





def initialise():
	global DISPLAY_SURF, CURRENT_SCREEN, SCREENS

	initDisplay()

	# Initialise all screens
	SCREENS = {
		LoginScreen.name: 	LoginScreen(DISPLAY_SURF),
		Desktop.name:		Desktop(DISPLAY_SURF)
	}
	

	print("UI Initialised")



def initDisplay():
	pygame.display.set_caption('Super L❤️ve')
	global DISPLAY_SURF
	DISPLAY_SURF = pygame.display.set_mode((0,0),pygame.FULLSCREEN)

	displayInfo = pygame.display.Info()
	DISPLAY_HEIGHT = displayInfo.current_h
	DISPLAY_WIDTH  = displayInfo.current_w
#initDisplay

def setCurrentScreen(screenName, value):
	CURRENT_SCREEN = screenName
	SCREENS[CURRENT_SCREEN] = value
#initDisplay

def getScreen(screenName):
	return SCREENS[screenName]
#getScreen

def getCurrentScreen():
	return CURRENT_SCREEN
#getCurrentScreen

def displayCurrentScreen():
	SCREENS[CURRENT_SCREEN].display() # if you get a key error here make sure 
	# that you've initialised your screen and have added it to SCREENS dict
#displayCurrentScreen




class fakeWindow:
	def __init__(self, name):
		self.name = name

	def displayWindow(self, winSize, winPos):
		# window background
		background = pygame.Surface(winSize)

		if self.name == SuperLove:	background.fill(INDIGO)
		elif self.name == Chatter:	background.fill(BLUE)

		else: raise Exception("Unrecognised Window")

		DISPLAY_SURF.blit(background, winPos)

		titleBar = pygame.Surface((winSize[0], 50))
		titleBar.fill(BLACK)
		DISPLAY_SURF.blit(titleBar, (175,0))
#drawWindow()


def createPlaceholderApp(name, windowManager):
	def link():
		print(f"Executing {name} link")
		# global SCREENS
		winSize = SCREENS['Desktop'].windowManager.winSize
		winPos = SCREENS['Desktop'].windowManager.winPos

		windowManager.openWindows = []
		windowManager.openWindows.append(fakeWindow(name))

	return link


def initIcons(windowManager):
	# Create app links
	contactsLink = windowManager.getAppLink(ContactsApp.ContactsApp)

	# Temp
	superLoveLink = createPlaceholderApp(SuperLove, windowManager)
	chatterLink = createPlaceholderApp(Chatter, windowManager)

	# Create App Icons
	contactsIcon  = UIElements.Icon(Contacts, contactsLink, thumbnail='assets/contacts.png')
	superloveIcon = UIElements.Icon(SuperLove, superLoveLink, thumbnail='assets/superlove.png')
	chatterIcon   = UIElements.Icon(Chatter, chatterLink, thumbnail='assets/chatter.png')

	# put icons in a list
	global DESKTOP_ICONS, BASIC_FONT
	DESKTOP_ICONS = [contactsIcon, superloveIcon, chatterIcon]

	# scale the icons thumbnails to be the right size
	for icon in DESKTOP_ICONS:
		icon.thumbnail = pygame.transform.scale(icon.thumbnail, (THUMBNAIL_HEIGHT, THUMBNAIL_HEIGHT))

	# determine	icon positions
	heightBetweenIcons = 30
	widthBetweenIcons = 50
	# start from the top
	currentHeight = 0
	# space above the first icon
	currentHeight += heightBetweenIcons
	for i in range(3):
		# init thumbnail
		icon = DESKTOP_ICONS[i]
		# x coord
		icon.thumbnailPos = (widthBetweenIcons, currentHeight)

		thumbnail_width = icon.thumbnail.get_rect().w
		thumbnail_height = icon.thumbnail.get_rect().h

		currentHeight += THUMBNAIL_HEIGHT

		# init icon name below
		icon.name = UIElements.BASIC_FONT.render(icon.name_string+".exe", 1, WHITE)
		name_width = icon.name.get_rect().w
		name_height = icon.name.get_rect().h
		# centre name below thumbnail
		icon.namePos = (widthBetweenIcons + thumbnail_width/2 - name_width/2, currentHeight)

		currentHeight += name_height

		# add a collisionBox so that it can be clicked on
		icon.collisionBoxPos = (icon.namePos[0] - 10, icon.thumbnailPos[1] - 10)
		icon.collisionBox = pygame.Rect((icon.collisionBoxPos), (name_width + 20, thumbnail_height+name_height + 20))


		# leave a space
		currentHeight += heightBetweenIcons
#initIcons()