# GMTK Gamejam 2021

## How to 

### Compile code into an exe file
1. Make sure you are on Windows

   Unfortunately, you can't compile windows exe files on other platforms with pyinstaller :(

2. Make sure to have python and pyinstaller installed. 

    Python should be easy enough to get using the installer from https://www.python.org/downloads/  
    > If you already have python try skipping to step 3 and if it doesn't work uninstall using the downloaded installer and then follow insturctions below  
    
    1\. Open dowloaded installer  
    2\. Make sure to tick 'Add Python to PATH' and 'install for all users'     
    3\. Select 'Instal Now'


3. Download repository  
    Download as zip from here https://gitlab.com/ant0kneeguy/gmtk-gamejam-2021  

4. Open console with admin privileges
   1. Open the start menu and type cmd   
   2. Right-click 'Command prompt' and select 'Run as administrator  

>   The administrator part is not always necessary but can help if python is failing to find installed moduels.  

5. Build exe file
    1. Navigate to the unzipped repository folder using `cd` command  
    e.g. `cd C:\Users\Mark\Desktop\gmtk-gamejam-2021`
    2. Build using this pyinstaller command:  
    `pyinstaller SuperLove.spec --distpath .`
  


And that should be it! 

Message me if you have any issues or questions.  
Mark


